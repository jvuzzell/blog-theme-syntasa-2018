
// Logger

var colors = require( 'colors/safe' );

module.exports = {

  info: ( title, content, ...data ) => {
    console.log(
      colors.white( `[${ title }]` ),
      content, ...data
    );
  },

  output: ( title, from, to ) => {
    console.log(
      colors.white( `[${ title }]` ),
      colors.green( from ), '=>', colors.green( to )
    );
  },

  warn: ( title, content, ...data ) => {
    console.log(
      colors.yellow.bold( `[${ title }]` ),
      content, ...data
    );
  },

  error: ( title, content, ...data ) => {
    console.log(
      colors.red.bold( `[${ title }]` ),
      content, ...data
    );
  }
};
