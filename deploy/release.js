
// Release

const path = require( 'path' );
const config = require( './config.json' );
const client = require( '../utilities/remote.js' );
const logger = require( '../utilities/logger.js' );

const name = process.argv[ 2 ] || config.default;
const target = config.targets[ name ];
const remote = new client( target.user, target.host, target.env );
const stamp = +new Date();

logger.info( 'NPM', `Running the "release" task on target "${ name }"` );

// Create a new release directory with the time "stamp"

try {
  remote.exec( `mkdir -p ${ target.destination }/releases/${ stamp }` );
  logger.info( 'Release', `Created the "releases/${ stamp }" directory on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Release', `Unable to create the "releases/${ stamp }" directory on "${ name }"`, error.message );
  process.exit( 1 );
}

// Copy the build to the new release directory, excluding any running socket (.sock) files

try {
  logger.info( 'Release', `Copying "${ config.build }" to "${ target.destination }/releases/${ stamp }" on "${ name }" ...` );
  remote.copy( path.posix.join( config.build, '*' ), `${ target.destination }/releases/${ stamp }`, '*.sock' );
}
catch( error ) {
  logger.error( 'Release', `Unable to copy "${ config.build }" to "${ target.destination }/releases/${ stamp }" on "${ name }":`, error.message );
  process.exit( 1 );
}

// Reset the "current" symlink on the target to this new release

try {
  remote.exec( `if [ -h ${ target.destination }/current ]; then sudo unlink ${ target.destination }/current; fi` );
  remote.exec( `sudo ln -s ${ target.destination }/releases/${ stamp } ${ target.destination }/current` );
  logger.info( 'Release', `Reset the "current" symlink on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Release', `Unable to reset the "current" symlink on "${ name }":`, error.message );
  process.exit( 1 );
}

// Set permissions on the "current" release

try {
  remote.exec( `sudo chmod -R 775 ${ target.destination }/current` );
  logger.info( 'Release', `Set permissions on the "current" release` );
}
catch ( error ) {
  logger.error( 'Release', `Unable to set permissions on the "current" release:`, error.message );
  process.exit( 1 );
}

// Restart nginx to effect any outstanding changes (just to be sure)

try {
  remote.exec( `sudo systemctl restart nginx` );
  logger.info( 'Release', `Reloaded nginx on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Release', `Unable to reload nginx on "${ name }":`, error.message );
  process.exit( 1 );
}
