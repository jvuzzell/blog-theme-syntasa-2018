
// Prepare

const path = require( 'path' );
const config = require( './config.json' );
const client = require( '../utilities/remote.js' );
const logger = require( '../utilities/logger.js' );

const name = process.argv[ 2 ] || config.default;
const target = config.targets[ name ];
const remote = new client( target.user, target.host, target.env );

logger.info( 'NPM', `Running the "prepare" task on target "${ name }"` );

// Create the basic directories on the target

try {
  remote.exec( `mkdir -p ${ target.destination }/config ${ target.destination }/releases` );
  logger.info( 'Prepare', `Created the "config" and "release" directories on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Prepare', `Unable to create the "config" and "release" directories on ${ name }:`, error.message );
  process.exit( 1 );
}

// Upload a copy of the target configuration files

try {
  remote.copy( path.posix.join( target.config, '*' ), `${ target.destination }/config` );
  logger.info( 'Prepare', `Copied "${ target.config }/*" to "${ target.destination }/config" on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Prepare', `Unable to copy "${ target.config }/*" to "${ target.destination }/config" on "${ name }":`, error.message );
  process.exit( 1 );
}

// Link the nginx configuration file into "/etc/nginx/conf.d" on the target

try {
  remote.exec( `if [ -h /etc/nginx/conf.d/${ target.nginx } ]; then sudo unlink /etc/nginx/conf.d/${ target.nginx }; fi` );
  remote.exec( `sudo ln -s ${ target.destination }/config/${ target.nginx } /etc/nginx/conf.d/${ target.nginx }` );
  logger.info( 'Prepare', `Added the nginx conf to "/etc/nginx/conf.d/${ target.nginx }" on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Prepare', `Unable to add the nginx conf to "/etc/nginx/conf.d/${ target.nginx }" on "${ name }":`, error.message );
  process.exit( 1 );
}

// Restart nginx to add the vhost specified in the configuration file

try {
  remote.exec( `sudo systemctl restart nginx` );
  logger.info( 'Prepare', `Reloaded nginx on "${ name }"` );
}
catch ( error ) {
  logger.error( 'Prepare', `Unable to reload nginx on "${ name }":`, error.message );
  process.exit( 1 );
}