
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="">
  <meta name="description" content="">

  <!-- Google Tag Manager -->
  <!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TC8TRVV');</script> -->
  <!-- End Google Tag Manager -->

  <!-- Google Tag Manager -->

  <!-- Google Tag Manager -->
  <!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5SN8MD3');</script> -->
  <!-- End Google Tag Manager -->


  <!-- Global site tag (gtag.js) - Google Analytics -->

  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112626240-1"></script>

  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-112626240-1');
  </script> -->

  <meta property="fb:app_id"       content="144155056294767" />
  <meta property="og:url"          content="http://syntasa.uzzell.codes" />
  <meta property="og:type"         content="article" />
  <meta property="og:title"        content="Syntasa - " />
  <meta property="og:description"  content="In 2017, 65% of all US retail sales are involving a visit to the company's website. And as you may have already realised, your website is one of the most important sources of data when it comes to understanding your customers' wants, needs, and preferences. Assuming you have web analytics, this source of data gives you the opportunity to track and collect data on every single interaction a customer makes with your brand." />
  <meta property="og:image"        content="http://syntasa.uzzell.codes/../images/blog-image-1.jpg" />

  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="post-views.css">
  <title>Syntasa - Blog Landing Page</title>

  <link href="http://www.syntasa.com/wp-content/uploads/2017/03/syntasa_favicon@2x.png" rel="icon">
  <link href="http://www.syntasa.com/wp-content/uploads/2017/03/ipadsy@2x.png" rel="apple-touch-icon">
  <link href="http://www.syntasa.com/wp-content/uploads/2017/03/ipadsy120x120@2x.png" rel="apple-touch-icon" sizes="76x76" />
  <link href="http://www.syntasa.com/wp-content/uploads/2017/03/syntasa_ipad-favicon_76x76.png" rel="apple-touch-icon" sizes="120x120" />
  <link href="http://www.syntasa.com/wp-content/uploads/2017/03/ipadsy152x152@2x.png" rel="apple-touch-icon" sizes="152x152" />

</head>
<body>

  <div style="background-color: #000; height: 80px; width: 100%;">
    <div class="lock 1100" style="padding-bottom: 0;">
    <p class="white" style="line-height: 80px;">Placeholder</p>
  </div>
  <section class="stage background-image" style="background-image:url(images/blog-image-6.jpg); background-position: 50% 85%;">
    <div class="lock align-center">
      <div class="narrow-940 layout-center">
        <span class="green bold uppercase x-small loose-letter-spacing">Latest Post</span>
        <h1 class="x-large x-small-margin tight-letter-spacing">It's the Thought That&nbsp;Counts</h1>
        <p class="medium-margin x-small">Think about the feeling you get when someone you care about gives you a gift. Surprise... delight... overall, a pleasant feeling, right? That's because a gift is thoughtful and gives the impression that some time and effort went into providing that special gifting experience.</p>
        <button class="green-background button padded rounder gradient-hover" data-category="analytics">Read More</button>
      </div>
    </div>
    <div class="screen"></div>
  </section>

  <section class="blog-controls">
    <div class="lock">
      <div class="row">
        <div class="cell">
          <button class="grid-view-icon before green"></button>
          <button class="list-view-icon before grey"></button>
          <button class="category-filter button round padded green border after dropdown-icon">Categories</button>
        </div>
        <div class="cell">
          <form id="blog-search">
            <input id="blog-search-text" type="text" value="" placeholder="Search Blog">
            <button id="blog-search-submit" type="submit" class="before black search-icon" label="Search Blog"></button>
          </form>
        </div>
      </div>

      <div id="categories" class="filter-categories" data-state="closed">
        <div class="wrapper">
          <button class="grey-background button padded rounder" data-category="analytics">Analytics</button>
          <button class="grey-background button padded rounder" data-category="excutives">Excutives</button>
          <button class="grey-background button padded rounder" data-category="strategy">Strategy</button>
          <button class="grey-background button padded rounder" data-category="retargetting">Retargetting</button>
          <button class="grey-background button padded rounder" data-category="events">Government</button>
          <button class="grey-background button padded rounder" data-category="events">Behavioral Data</button>
          <button class="grey-background button padded rounder" data-category="events">Interviews</button>
          <button class="grey-background button padded rounder" data-category="events">Retargetting</button>
          <button class="grey-background button padded rounder" data-category="events">Events</button>
        </div>
      </div>

    </div>
  </section>

  <section>
    <div id="landing-page-posts" class="lock">
      <div id="posts" data-view="grid">

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-1.jpg)">
            <a href="blog/unlocking-the-value-in-your-clickstream-data.html" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Unlocking the Value In Your Clickstream Data</h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Unlocking the Value In Your Clickstream&nbsp;Data</h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>In 2017, 65% of all US retail sales are involving a visit to the company's website. And as you may have already realised, your website is one of the most important sources of data when it comes to understanding your customers' wants, needs, and preferences. Assuming you have web analytics, this source of data gives you the opportunity to track and collect data on every single interaction a customer makes with your brand.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button  social-cta-button slide-in share-linkedin" href="https://www.linkedin.com/shareArticle?url=http://syntasa.uzzell.codes/blog/unlocking-the-value-in-your-clickstream-data.html" target="_blank" rel="noopener noreferrer"></a>
                <a class="twitter-green-icon button  social-cta-button slide-in share-twitter" href="https://twitter.com/intent/tweet?text=status=http://syntasa.uzzell.codes/blog/unlocking-the-value-in-your-clickstream-data.html" target="_blank" rel="noopener noreferrer"></a>
                <a class="facebook-green-icon button  social-cta-button slide-in share-facebook" href="https://www.facebook.com/sharer.php?u=http://syntasa.uzzell.codes/blog/unlocking-the-value-in-your-clickstream-data.html" target="_blank" rel="noopener noreferrer"></a>
                <a class="email-green-icon button  social-cta-button slide-in share-email" href="mailto:?subject=Syntasa%20Just%20Published%20this%20Great%20Article" target="_blank" rel="noopener noreferrer"></a>
                <button class="share-green-icon button social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <p><a class="green bold" href="blog/unlocking-the-value-in-your-clickstream-data.html">READ MORE ></a></p>
              </div>
            </div>
          </div>
        </div>


        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-2.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">6 Tips for A New Chief Data Officer</h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">6 Tips for A New Chief Data Officer</h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>A guy walks into a bar. The bartender asks, "What'll it be today?" The guy answers: "Gee, I just don't know." Pretty boring joke, right? The same letdown occurs when a new Chief Data Officer has no clear strategy for the future.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>


        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-7.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Will GDPR Rule You or Will You Rule It?</h2>
            </div>

          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Will GDPR Rule You or Will You Rule&nbsp;It?</h2>

            </div>

            <div class="row excerpt medium-margin">
              <p>As you've probably heard, starting next year any company that stores data on citizens of the European Union will have to comply with a stringent set of new data privacy rules called General Data Protection Regulation (GDPR). The deadline to comply is fast approaching. GDPR will apply in the UK, and the 28 member states of the European Union, beginning May 25, 2018.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-3.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Eric Siegel on the State of Predictive Analytics</h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Eric Siegel on the State of Predictive Analytics</h2>
              <p class="grey semibold">By <a href="#" class="grey author">Kerry Hew</a></p>
            </div>

            <div class="row excerpt medium-margin">
              <p>For me, it most definitely is Eric Siegel. If you don't already know, Eric Siegel, Ph.D., is the founder of the Predictive Analytics World conference series, the Executive Editor of Predictive Analytics Times, and author of Predictive Analytics: The Power to Predict Who Will Click, Buy, Lie, or Die (Revised and Updated).</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-8.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Retailers Can Have Their Bricks and Click Them Too </h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Retailers Can Have Their Bricks and Click Them Too </h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>Despite what you might have read, retail is not dying. Sure, brick-and-mortar retailers today face significant competition from their digital counterparts. But with the right omni-channel strategy, they can outperform online-only stores by providing the best of both worlds – the convenience of online e-commerce along with the human experience of physical stores.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-4.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Data That Stays Together, Works Together </h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Data That Stays Together, Works Together </h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>When it comes to data analytics, marketers are missing the forest for the trees. If you think your company's most marketable data source lies in your enterprise data, think again. Your company is sitting on a gold mine of customer data, siloed in different departments, just waiting to be integrated and activated.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-5.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Are Homemade Predictive Behavioral Analytics Applications Better? </h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Are Homemade Predictive Behavioral Analytics Applications Better? </h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>When it comes to data analytics, marketers are missing the forest for the trees. If you think your company's most marketable data source lies in your enterprise data, think again. Your company is sitting on a gold mine of customer data, siloed in different departments, just waiting to be integrated and activated.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-9.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">One Man's Trash is Another Man's Data Trove</h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">One Man's Trash is Another Man's Data Trove</h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>Companies have been hoarding too much data on consumer demographics. There is an over reliance on demographic data for consumer insights and business decisions. Luckily, a select group of companies are uncovering a data trove.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>


        <div class="post">
          <div class="featured-image background-image medium-margin" style="background-image:url(images/blog-image-7.jpg)">
            <a href="" class="click-post"></a>
            <div class="screen"></div>
            <div class="overlay">
              <h2 class="large loose-letter-spacing x-small-margin">Will GDPR Rule You or Will You Rule It?</h2>
            </div>
          </div>

          <div class="text-wrapper">
            <div class="row title-block medium-margin">
              <h2 class="large loose-letter-spacing x-small-margin">Will GDPR Rule You or Will You Rule&nbsp;It?</h2>
            </div>

            <div class="row excerpt medium-margin">
              <p>As you've probably heard, starting next year any company that stores data on citizens of the European Union will have to comply with a stringent set of new data privacy rules called General Data Protection Regulation (GDPR). The deadline to comply is fast approaching. GDPR will apply in the UK, and the 28 member states of the European Union, beginning May 25, 2018.</p>
            </div>

            <div class="row post-actions">
              <div class="social cell">
                <a class="linkedin-green-icon button social-cta-button slide-in share-linkedin" href=""></a>
                <a class="twitter-green-icon button social-cta-button slide-in share-twitter" href=""></a>
                <a class="facebook-green-icon button social-cta-button slide-in share-facebook" href=""></a>
                <a class="email-green-icon button social-cta-button slide-in share-email" href=""></a>
                <button class="share-green-icon butto social-cta-button share-button"></button>
              </div>
              <div class="read-more cell">
                <a class="green bold" href="#">READ MORE ></a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section>
    <div id="load-more" class="lock align-center large-margin">
      <button class="green-background button padded rounder gradient-hover dropshadow center" data-category="analytics">Load More</button>
    </div>
  </section>

  <script src="main.js"></script>
  <!-- Google Tag Manager (noscript) -->
  <!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TC8TRVV"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
  <!-- End Google Tag Manager (noscript) -->

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SN8MD3"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
</body>
</html>
