(function(){

  var listViewButton = document.querySelector( '.list-view-icon' ),
      gridViewButton = document.querySelector( '.grid-view-icon' ),
      categoryFilterButton = document.querySelector( '.category-filter' ),
      categoryFilters = document.querySelector('.filter-categories'),
      postActionContainers = document.querySelectorAll( '.post-actions' );

  var i = 0,
      n = 0;

  window.addEventListener( 'resize', windowResizeEvents );

  if( listViewButton != null ) {
    listViewButton.addEventListener( 'click', function() { togglePostView( this, gridViewButton ) } );
  }

  if( gridViewButton != null ) {
    gridViewButton.addEventListener( 'click', function() { togglePostView( this, listViewButton ) } );
  }

  if( categoryFilterButton != null ) {
    calculateCategoryHeight();

    setTimeout( function(){ categoryFilters.setAttribute( "data-state", "closed" ); }, 10);

    categoryFilterButton.addEventListener( 'click', function() {
      if( categoryFilters.getAttribute( 'data-state' ) == 'open' ) {
        categoryFilters.setAttribute( 'data-state', 'closed' );
        removeClass( categoryFilterButton, 'green-background', 'string' );
      } else {
        categoryFilters.setAttribute( "data-state", "open" );
        addClass( categoryFilterButton, 'green-background', 'string' );
      }
    });
  }

  for( i = 0; i < postActionContainers.length; i++ ) {
    var shareButton = postActionContainers[i].querySelector( '.social .share-button' ),
        socialActions = postActionContainers[i].querySelectorAll('.social a.slide-in');

    activateSocialIconAnimation( shareButton, socialActions );
  }

})();

function addClass( elemsAddClassTo, classesToAdd, dataType ) {
  if( dataType == 'array' ) {
    for( var i = 0; i < elemsAddClassTo.length; i++ ){
      elemsAddClassTo[i].className = elemsAddClassTo[i].className + " " + classesToAdd;
    }
  } else if( dataType == 'string' ) {
    elemsAddClassTo.className = elemsAddClassTo.className + " " + classesToAdd;
  }
}

function removeClass( elemArray, classesToRemove, dataType ) {
  var classes = classesToRemove.split( ' ' );
  var elemsRemoveClassFrom = [];

  if( dataType == 'string' ) {
    elemsRemoveClassFrom[0] = elemArray;
  } else if( dataType == 'array' ) {
    elemsRemoveClassFrom = elemArray;
  }

  for( var x = 0; x < elemsRemoveClassFrom.length; x++ ) {
    for( var y = 0; y < classes.length; y++ ) {
      var exp = [
        new RegExp( " "+classes[y],'g' ),
        new RegExp( classes[y]+" ",'g' ),
        new RegExp( classes[y],'g' )
      ];

      for( var z = 0; z < exp.length; z++ ) {
        elemsRemoveClassFrom[x].className = elemsRemoveClassFrom[x].className.replace( exp[z], '' );
      }
    }
  }
}

function hasClass(elem, classesToFind){
  var findThese = new RegExp(classesToFind,'g');
  return findThese.test( elem.className );
}

function toggleActive( element ) {
  if( hasClass( element, 'active' ) ) {
    removeClass( element, 'active', 'string' );
  } else {
    addClass( element, 'active', 'string' );
  }
}

function windowResizeEvents( e ) {
  var landingPagePostsElem = document.querySelector( '#landing-page-posts #posts[data-view]' ),
      relatedPostsElem = document.querySelector( '#related-posts #posts[data-view]' ),
      filterCategoriesElem = document.querySelector('.filter-categories');

  if( filterCategoriesElem != null ) {
    calculateCategoryHeight();
  }

  windowWidth = Math.max(
    window.innerWidth,
    window.outerWidth,
    document.documentElement.clientWidth
  );

  // if( windowWidth > 960 ) {
  //   if( relatedPostsElem != null && relatedPostsElem.getAttribute( 'data-view' ) == 'list' ) {
  //     selectGridView();
  //   }
  //
  //   if( landingPagePostsElem != null && landingPagePostsElem.getAttribute( 'data-view' ) == 'list' ) {
  //     selectGridView();
  //   }
  // }
}

function calculateCategoryHeight() {
  document.querySelector( '.filter-categories' ).style.height = ( document.querySelector(".filter-categories .wrapper").clientHeight ) + 'px';
}

function selectListView() {
  document.querySelector( '#posts[data-view]' ).setAttribute( 'data-view', 'list' );

  if( document.querySelector( '.list-view-icon' ) != null ) {
    togglePostView( document.querySelector( '.list-view-icon' ), document.querySelector( '.grid-view-icon' ) );
  }
}

function selectGridView() {
  document.querySelector( '#posts[data-view]' ).setAttribute( 'data-view', 'grid' );
  togglePostView( document.querySelector( '.grid-view-icon' ), document.querySelector( '.list-view-icon' ) );
}

function togglePostView( activeViewButton, inActiveViewButton ) {
  addClass( activeViewButton, 'green', 'string' );
  removeClass( activeViewButton, 'grey', 'string' );

  addClass( inActiveViewButton, 'grey', 'string' );
  removeClass( inActiveViewButton, 'green', 'string' );

  if( activeViewButton == document.querySelector( '.list-view-icon' ) ) {
    document.querySelector( '#posts[data-view]' ).setAttribute( 'data-view', 'list' );
  } else {
    document.querySelector( '#posts[data-view]' ).setAttribute( 'data-view', 'grid' );
  }

}

function activateSocialIconAnimation( trigger, elements ) {
  trigger.addEventListener( 'click', function() {
    for( n = 0; n < elements.length; n++ ) {
      toggleActive( elements[n] );
    }
  });
}
